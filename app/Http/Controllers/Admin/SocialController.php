<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Social;
use Illuminate\Support\Facades\Session;

class SocialController extends Controller
{
    // Site Settings
    public function social(){
        $social = Social::first();
        return view('admin.social.social', compact('social'));
    
    }

    public function SocialUpdate(Request $request, $id){
        $data = $request->all();
        $social = Social::first();
        $social->facebook = $data['facebook'];
        $social->youtube = $data['youtube'];
        $social->twitter = $data['twitter'];
        $social->instagram = $data['instagram'];
        $social->linkedin = $data['linkedin'];
        $social->save();
        Session::flash('success_message', 'Social Settings has been Updated Successfully');
        return redirect()->back();
    }
}
