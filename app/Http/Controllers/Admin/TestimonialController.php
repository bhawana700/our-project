<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class TestimonialController extends Controller
{
    // Index Page
    public function index(){
        $testimonials = Testimonial::latest()->get();
        return view('admin.testimonial.index', compact('testimonials'));
    }

     // Index Page
     public function add(){
        return view('admin.testimonial.add');
    }

    // Store
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'name' => 'required|max:255',
            'position' => 'required',
            'image' => 'required',
            'details' => 'required'
             ];
        $customMessages = [
            'name.required' => ' Name is required',
            ];
        $this->validate($request, $rules, $customMessages);
        $testimonial = new Testimonial();
        $testimonial->name = $data['name'];
        $testimonial->position = $data['position'];
        $testimonial->details = $data['details'];

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
               $filename = $random .'.'.$extension;
               $image_path = 'public/uploads/testimonial/' . $filename;
               Image::make($image_tmp)->save($image_path);
               $testimonial->image = $filename;
            }
        }

        $testimonial->save();
        Session::flash('success_message', 'Testimonial has been Added Successfully');
        return redirect()->back();
   
        

        }
        // Edit Page
     public function edit($id){
         $testimonial = Testimonial::findOrfail($id);
        return view('admin.testimonial.edit', compact('testimonial'));
    }

    // Store
    public function update(Request $request, $id){
        $data = $request->all();
        $rules = [
            'name' => 'required|max:255',
            'position' => 'required',
            'details' => 'required'
             ];
        $customMessages = [
            'name.required' => ' Name is required',
            ];
        $this->validate($request, $rules, $customMessages);
        $testimonial = Testimonial::findOrfail($id);
        $testimonial->name = $data['name'];
        $testimonial->position = $data['position'];
        $testimonial->details = $data['details'];

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
               $filename = $random .'.'.$extension;
               $image_path = 'public/uploads/testimonial/' . $filename;
               Image::make($image_tmp)->save($image_path);
               $testimonial->image = $filename;
            }
        }

        $testimonial->save();
        Session::flash('success_message', 'Testimonial has been Updated Successfully');
        return redirect()->back();
   
        

        }
        public function delete($id){
            $testimonial = Testimonial::findOrfail($id);
            $testimonial->delete();
            $image_path = 'public/uploads/testimonial/';
            if(file_exists($image_path.$testimonial->image)){
                unlink($image_path.$testimonial->image);
            }
            Session::flash('success_message', 'Testimonial has been Deleted Successfully');
            return redirect()->route('testimonial.index');
    
        }
    
}
