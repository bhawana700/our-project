<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use App\Models\Pricing;
use Illuminate\Support\Str;

class PricingController extends Controller
{
    // Index Page
    public function index(){
        $pricings = Pricing::all();
     return view('admin.price.index', compact('pricings'));
    }
     // Add
     public function add(){
         return view('admin.price.add');
     }

     // Store
     public function store(Request $request){
         $data = $request->all();

         $rules = [
            'title' => 'required|max:255',
            'price' => 'required',
            'image' => 'required',
            'features' => 'required',              
        ];
        $customMessages = [
            'title.required' => ' Pricing Title is required',
            'price.required' => ' Price is required',
           'image.required' => ' Pricing Image Title is required',
            'features.required' => 'Pricing Features is required',
            'title.max' => 'you are not allowed to enter more than 255 characters',
           
        ];
        $this->validate($request, $rules, $customMessages);
        $pricing = new Pricing();
        $pricing->title = $data['title'];
        $pricing->price = $data['price'];
        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
               $filename = $random .'.'.$extension;
               $image_path = 'public/uploads/price/' . $filename;
               Image::make($image_tmp)->save($image_path);
               $pricing->image = $filename;
            }
        }

        $features[] = $request->features;
        $featuresAll = json_encode($features);
        $pricing->features = $featuresAll;
        $pricing->save();
        Session::flash('success_message', 'Pricing has been Added Successfully');
        return redirect()->route('pricing.index');
       
     }

     // Edit
     public function edit($id){
         $pricing = Pricing::findorfail($id);
         return view('admin.price.edit', compact('pricing'));

     }

      // Update
      public function update(Request $request, $id){
        $data = $request->all();

        $rules = [
           'title' => 'required|max:255',
           'price' => 'required',
          
           'features' => 'required',              
       ];
       $customMessages = [
           'title.required' => ' Pricing Title is required',
           'price.required' => ' Price is required',
          'image.required' => ' Pricing Image Title is required',
           'features.required' => 'Pricing Features is required',
           'title.max' => 'you are not allowed to enter more than 255 characters',
          
       ];
       $this->validate($request, $rules, $customMessages);
       $pricing = Pricing::findorfail($id);
       $pricing->title = $data['title'];
       $pricing->price = $data['price'];
       $random = Str::random(10);
       if($request->hasFile('image')){
           $image_tmp = $request->file('image');
           if($image_tmp->isValid()){
               $extension = $image_tmp->getClientOriginalExtension();
              $filename = $random .'.'.$extension;
              $image_path = 'public/uploads/price/' . $filename;
              Image::make($image_tmp)->save($image_path);
              $pricing->image = $filename;
           }
       }

       $features[] = $request->features;
       $featuresAll = json_encode($features);
       $pricing->features = $featuresAll;
       $pricing->save();
       Session::flash('success_message', 'Pricing has been Updated Successfully');
       return redirect()->route('pricing.index');
      
    }

    public function delete($id){
        $pricing = Pricing::findOrfail($id);
        $pricing->delete();
        Session::flash('success_message', 'Pricing has been Deleted Successfully');
        return redirect()->route('pricing.index');

    }

}
