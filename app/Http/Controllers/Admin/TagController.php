<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class TagController extends Controller
{
     // Index Page
     public function index(){
        $tags = Tag::orderBy('tag_name', 'ASC')->get();
        return view('admin.cms.tags.index', compact('tags'));
    }
    // Store Caregory
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'tag_name' => 'required|max:255|unique:tags,tag_name',
             ];
        $customMessages = [
            'tag_name.required' => ' Tag Name is required',
            'tag_name.unique' => 'Tag Name already exists in our database'
            ];
        $this->validate($request, $rules, $customMessages);
        $tag = new Tag();
        $tag->tag_name = strtolower($data['tag_name']);
        $tag->slug = Str::slug($data['tag_name']);
        $tag->save();
        Session::flash('success_message', 'Tag has been Added Successfully');
        return redirect()->back();
   
    }

    // Update Tag
    public function update(Request $request, $id){
        $data = $request->all();
        $tag = Tag::findOrfail($id);
        $rules = [
            'tag_name' => 'required|max:255|unique:tags,tag_name,'.$category->id,
             ];
        $customMessages = [
            'tag_name.required' => ' Tag Name is required',
            'tag_name.unique' => 'Tag Name already exists in our database'
            ];
        $this->validate($request, $rules, $customMessages);
        $tag->tag_name = $data['tag_name'];
        $tag->slug = Str::slug($data['tag_name']);
        $tag->save();
        Session::flash('success_message', 'Tag has been Updated Successfully');
        return redirect()->back();
   
    }
    public function delete($id){
        $tag = Tag::findOrfail($id);
        $tag->delete();
        Session::flash('success_message', 'Tag has been Deleted Successfully');
        return redirect()->back();
    }

}
