<?php

namespace Database\Seeders;
use App\Models\Admin;
use App\Models\Theme;
use Illuminate\Database\Seeder;
use App\Models\SiteSetting;
use App\Models\Social;
use App\Models\AboutUsPage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

      
        Admin::insert([
            'name' => 'Bhawana Niroula',
            'email' => 'bhawananiroula12@gmail.com',
            'password' => bcrypt('password'),
        ]);
        Admin::insert([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
        ]);


        Theme::insert([
            'website_name' => "Tech Coderz",
            'website_tagline' =>"Inspire the Next",
        ]);

           SiteSetting::insert([
           'email' => ('info@project.com')
        ]);

        Social::insert([
            'facebook' => ''
        ]);

        AboutUsPage::insert([
            'page_name' => 'About Us',
            'page_title' => 'Manages IT Service Across Various Business',
            'page_subtitle' => '',
            'page_content' => ''
        ]);


       
    }
}
