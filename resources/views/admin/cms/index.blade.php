@extends('admin.includes.admin_design')

@section('content')

<!--start content-->
<main class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Blog Management</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">View All Blogs</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						
							<a href="{{ route('blog.add')}}" class="btn btn-primary"> <i class="bi bi-plus"></i>Add New Blog</a>
							</div>
				</div>
				<!--end breadcrumb-->
				
				<hr/>

				<div class="card">
					@include('admin.includes._message')
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>S.N.</th>
										<th>Image</th>
										<th>Blog Title</th>
                                        <th>Category</th>
                                        <th>Tags</th>
                                        <th>View Count</th>
										<th>Status</th>
                                        <th>Created Date</th>
										<th>Actions</th>
									</tr>
								</thead>
							<tbody>
							@foreach($blogs as $blog)
                            <tr>
                                <td>{{$loop->index + 1}}</td>
                                <td>
                                    <img src="{{ asset('public/uploads/blog/'.$blog->image)}}" alt="{{$blog->blog_title}}" width="100px">
                                </td>
                                <td>{{ $blog->blog_title}}</td>
                                <td>{{ $blog->categories->category_name}}</td>
                                <td>
									@php $blog_tags = $blog->tags->sortBy('tag_name')->pluck('id'); @endphp
									@foreach($blog_tags as $group)
									<span class="badge bg-secondary">{{ \App\Models\Tag::find($group)->tag_name}}</span>
									@endforeach
								</td>
                                <td>{{ $blog->view_count}}</td>
                                <td>
											@if($blog->status == 'published')
											<span class="badge bg-primary">Published</span>
											@else
											<span class="badge bg-danger">Draft</span>
											@endif
                                            </td>
                                            <td>{{ $blog->created_at->diffForHumans()}}</td>
                                            <td>
											<a href="{{ route('blog.edit', $blog->id)}}" class="btn btn-sm btn-info"style="color: white" >
												<i class="bx bx-edit-alt"></i>
											</a>

											<a href="javascript;" rel="{{$blog->id}}" rel1="delete-blog" class="btn btn-sm btn-danger btn-delete"style="color: white" >
												<i class="bx bx-trash-alt"></i>
											</a>
										</td>
									
                            </tr>
                            @endforeach
                            </tbody>
							
							</table>
						</div>
					</div>
				</div>
				
				
				
			</main>
       <!--end page main-->

@endsection

@section('js')
<script>
    $('body').on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var SITEURL = '{{ URL::to('') }}';

            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
              });

</script>
@endsection