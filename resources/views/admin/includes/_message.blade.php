
                          @if(Session::has('error_message'))
                        <div class="d-flex align-items-center">
                      <div class="fs-3 text-danger"><i class="bi bi-x-circle-fill"></i>
                      </div>
                      <div class="ms-3">
                        <div class="text-danger">
                          {{ Session::get('error_message') }}
                        </div>
                      </div>
                      @endif

                      @if(Session::has('info_message'))
                        <div class="d-flex align-items-center">
                      <div class="fs-3 text-info"><i class="bi bi-x-circle-fill"></i>
                      </div>
                      <div class="ms-3">
                        <div class="text-info">
                          {{ Session::get('info_message') }}
                        </div>
                      </div>
                      @endif

                      @if(Session::has('success_message'))
                        <div class="d-flex align-items-center">
                      <div class="fs-3 text-success"><i class="bi bi-x-circle-fill"></i>
                      </div>
                      <div class="ms-3">
                        <div class="text-info">
                          {{ Session::get('success_message') }}
                        </div>
                      </div>
                      @endif
                    </div>
                        @if($errors->any())
                      <div class="alert alert-danger">
                        <ul>
                          @foreach($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                        </div>
                        @endif