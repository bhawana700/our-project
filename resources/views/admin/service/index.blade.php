@extends('admin.includes.admin_design')

@section('content')

<!--start content-->
<main class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Service Management</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="{{ route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">View All Services</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						
							<a href="{{ route('service.add')}}" class="btn btn-primary"> <i class="bi bi-plus"></i>Add New Service</a>
							</div>
				</div>
				<!--end breadcrumb-->
				
				<hr/>

				<div class="card">
					@include('admin.includes._message')
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>S.N.</th>
										<th>Image</th>
										<th>Service Title</th>
                                        <th>Category</th>
                                        <th>Tags</th>
                                        <th>View Count</th>
										<th>Status</th>
                                        <th>Created Date</th>
										<th>Actions</th>
									</tr>
								</thead>
							<tbody>
							
                            </tbody>
							
							</table>
						</div>
					</div>
				</div>
				
				
				
			</main>
       <!--end page main-->

@endsection

@section('js')
<script>
    $('body').on('click', '.btn-delete', function (event) {
            event.preventDefault();

            var SITEURL = '{{ URL::to('') }}';

            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
              });

</script>
@endsection