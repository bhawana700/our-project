@extends('front.includes.front_design')

@section('site_title')
{{ $about->page_name}} - {{ $theme->website_name}} - {{ $theme->website_tagline}}
@endsection

@section('content')



        <!-- start page title area-->
        <div class="page-title-area bg-thin">
            <div class="container">
                <div class="page-title-content">
                    <h1>about</h1>
                    <ul>
                        <li class="item"><a href="{{ route('Index')}}">Home</a></li>
                        <li class="item"><a href="javascript:">{{ $about->page_name}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="shape">
                <span class="shape1"></span>
                <span class="shape2"></span>
                <span class="shape3"></span>
                <span class="shape4"></span>
            </div>
        </div>
        <!-- end page title area -->

        <!--start about section-->
        <section id="about" class="about-section ptb-100 bg-thin">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-sm-12 pb-30">
                        <div class="section-title">
                            <span class="subtitle">{{ $about->page_title}}</span>
                            <h2>{{ $about->page_subtitle}}</h2>
                        </div>
                        <div class="about-text-blc">
                            {!! $about->page_content !!}
                        </div>
                      
                    </div>
                    <div class="col-lg-6 col-sm-12 pb-30">
                        <div class="about-img">
                            <div class="grid-img">
                                <div class="grid-img-inner">
                                    <img src="{{asset('public/uploads/'.$about->image_1)}}" class="image-responsive" alt="office_image" />
                                </div>
                                <div class="grid-img-inner">
                                    <img src="{{asset('public/uploads/'.$about->image_2)}}" class="image-responsive" alt="office_image" />
                                </div>
                                <div class="grid-img-inner">
                                    <img src="{{asset('public/uploads/'.$about->image_3)}}" class="image-responsive" alt="office_image" />
                                </div>
                                <div class="grid-img-inner">
                                    <img src="{{asset('public/uploads/'.$about->image_4)}}" class="image-responsive" alt="office_image" />
                                </div>
                            </div>
                            <div class="logo-overlay">
                            <img src="{{ asset('public/frontend/assets/img/logos/logo.png')}}" alt="logo_without_slogan" />
                            </div>
                            <div class="shape">
                            <img src="{{ asset('public/frontend/assets/img/resource/shape_2.png')}}" alt="shape" class="shape-inner" />
                                <img src="{{ asset('public/frontend/assets/img/resource/shape_4.png')}}" alt="shape" class="shape-inner" />
                                <img src="{{ asset('public/frontend/assets/img/resource/shape_2.png')}}" alt="shape" class="shape-inner" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end about section-->

        <!-- start team section-->
        <section class="team-section ptb-100 bg-light">
            <div class="container">
                <div class="section-title">
                    <span class="subtitle">OUR Team</span>
                    <h2>Meet Awesome People</h2>
                    
                </div>
                <div class="team-slider owl-carousel">
                    @foreach($teams as $team)
                    <div class="team-item">
                        <div class="team-image">
                            <img src="{{ asset('public/uploads/team/'.$team->image)}}" alt="{{ $team->name}}" />
                        </div>
                        <div class="team-content">
                            <h5>
                                <a href="#">{{ $team->name}}</a>
                            </h5>
                            <p class="mb-2">{{ $team->designation->title}}</p>
                            <div class="social-link">
                                <a href="{{ $team->facebook}}" class="bg-tertiary" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#" class="bg-success" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#" class="bg-danger" target="_blank">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                                <a href="#" class="bg-info" target="_blank">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                 
                </div>
            </div>
        </section>
        <!-- end team section -->

      
@endsection