@extends('front.includes.front_design')

@section('site_title')
Project - {{ $theme->website_name}} - {{ $theme->website_tagline}}
@endsection

@section('content')


        <!-- start page title area-->
        <div class="page-title-area bg-thin">
            <div class="container">
                <div class="page-title-content">
                    <h1>Projects</h1>
                    <ul>
                        <li class="item"><a href="index.html">Home</a></li>
                        <li class="item"><a href="projects.html">Projects</a></li>
                    </ul>
                </div>
            </div>
            <div class="shape">
                <span class="shape1"></span>
                <span class="shape2"></span>
                <span class="shape3"></span>
                <span class="shape4"></span>
            </div>
        </div>
        <!-- end page title area -->

        <!-- start gallery section -->
        <section class="gallery-section ptb-100 bg-white">
            <div class="container">
                <div class="section-title">
                    @foreach($project as $project)
                    <h2>See Our Recent Case Study</h2>
                    <p>Does any industry face a more complex audience journey and marketing sales process than B2B technology.Does any industry face a more complex audience.</p>
                </div>
                @endforeach
                <div class="gallery-slider owl-carousel">
                    <div class="gallery-item">
                        <div class="gallery-image"><img src="{{ asset('public/uploads/project/'. $project->image)}}" alt="gallery-member" /></div>
                        <div class="gallery-content">
                            <h3>
                                <a href="project-details.html">UI/UX Design</a>
                            </h3>
                        </div>
                    </div>
                
                </div>
            </div>
        </section>
        <!-- end gallery section -->

       @endsection