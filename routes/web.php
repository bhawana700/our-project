<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Index Page
Route::get('/', [\App\Http\Controllers\Front\IndexController::class, 'Index'])->name('Index');
//About Us
Route::get('/about-us', [\App\Http\Controllers\Front\FrontEndController::class, 'aboutUs'])->name('aboutUs');
// Testimonials
Route::get('/testimonial', [\App\Http\Controllers\Front\FrontEndController::class, 'testimonial'])->name('testimonial');
// Blog
Route::get('/blog', [\App\Http\Controllers\Front\FrontEndController::class, 'blog'])->name('blog');
// Blog Details
Route::get('/blog/{slug}', [\App\Http\Controllers\Front\FrontEndController::class, 'blogDetail'])->name('blogDetail');
// Category Based Blogs
Route::get('/category/{slug}', [\App\Http\Controllers\Front\FrontEndController::class, 'categoryBlog'])->name('categoryBlog');
// Pricings
Route::get('/pricings', [\App\Http\Controllers\Front\FrontEndController::class, 'pricing'])->name('pricing');
// Projects
Route::get('/projects', [\App\Http\Controllers\Front\FrontEndController::class, 'project'])->name('project');


Route::prefix('/admin')->group(function(){

// Admin Login
Route::get('/login', [\App\Http\Controllers\Admin\AdminLoginController::class,'adminLogin'])->name('adminLogin');
Route::post('/login', [\App\Http\Controllers\Admin\AdminLoginController::class,'loginAdmin'])->name('loginAdmin');
Route::group(['middleware' => 'admin'], function(){
    // Admin Dashboard
Route::get('/dashboard', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminDashboard'])->name('adminDashboard');
  // Admin Profile
Route::get('/profile', [\App\Http\Controllers\Admin\AdminProfileController::class, 'adminProfile'])->name('adminProfile');
// Admin Profile Update
Route::post('/profile/update{id}', [\App\Http\Controllers\Admin\AdminProfileController::class, 'adminProfileUpdate'])->name('adminProfileUpdate');
// Delete Image
Route::get('/delete-image/{id}', [\App\Http\Controllers\Admin\AdminProfileController::class, 'deleteImage'])->name('deleteImage');
// Change Password
Route::get('/change-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'changePassword'])->name('changePassword');
// Check Current Password
Route::post('/check-password', [\App\Http\Controllers\Admin\AdminProfileController::class, 'chkUserPassword'])->name('chkUserPassword');
// Theme Settings
Route::get('/theme', [\App\Http\Controllers\Admin\ThemeController::class, 'theme'])->name('theme');
Route::post('/theme/{id}', [\App\Http\Controllers\Admin\ThemeController::class, 'themeUpdate'])->name('themeUpdate');

// Site Settings
Route::get('/settings', [\App\Http\Controllers\Admin\SettingController::class, 'settings'])->name('settings');
Route::post('/settings/{id}', [\App\Http\Controllers\Admin\SettingController::class, 'settingsUpdate'])->name('settingsUpdate');


// Site Settings
Route::get('/social', [\App\Http\Controllers\Admin\SocialController::class, 'social'])->name('social');
Route::post('/social/{id}', [\App\Http\Controllers\Admin\SocialController::class, 'socialUpdate'])->name('socialUpdate');

// About Us Page
Route::get('/about', [\App\Http\Controllers\Admin\PagesController::class, 'about'])->name('about');
Route::post('/about/{id}', [\App\Http\Controllers\Admin\PagesController::class, 'aboutUpdate'])->name('aboutUpdate');

// Banner Route
Route::get('/banner', [\App\Http\Controllers\Admin\BannerController::class, 'index'])->name('banner.index');
Route::get('/banner/add', [\App\Http\Controllers\Admin\BannerController::class, 'add'])->name('banner.add');
Route::post('/banner/store', [\App\Http\Controllers\Admin\BannerController::class, 'store'])->name('banner.store');
Route::get('/banner/edit/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'edit'])->name('banner.edit');
Route::post('/banner/update/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'update'])->name('banner.update');
Route::get('/delete-banner/{id}', [\App\Http\Controllers\Admin\BannerController::class, 'delete'])->name('banner.delete');


// Designation Route
Route::get('/designation', [\App\Http\Controllers\Admin\DesignationController::class, 'index'])->name('designation.index');
Route::post('/designation/store', [\App\Http\Controllers\Admin\DesignationController::class, 'store'])->name('designation.store');
Route::post('/designation/update/{id}', [\App\Http\Controllers\Admin\DesignationController::class, 'update'])->name('designation.update');
Route::get('/delete-designation/{id}', [\App\Http\Controllers\Admin\DesignationController::class, 'delete'])->name('designation.delete');


// Team Route
Route::get('/team', [\App\Http\Controllers\Admin\TeamController::class, 'index'])->name('team.index');
Route::get('/team/add', [\App\Http\Controllers\Admin\TeamController::class, 'add'])->name('team.add');
Route::post('/team/store', [\App\Http\Controllers\Admin\TeamController::class, 'store'])->name('team.store');
Route::get('/team/edit/{id}', [\App\Http\Controllers\Admin\TeamController::class, 'edit'])->name('team.edit');
Route::post('/team/update/{id}', [\App\Http\Controllers\Admin\TeamController::class, 'update'])->name('team.update');
Route::get('/delete-team/{id}', [\App\Http\Controllers\Admin\TeamController::class, 'delete'])->name('team.delete');


// Testimonial Route
Route::get('/testimonials', [\App\Http\Controllers\Admin\TestimonialController::class, 'index'])->name('testimonial.index');
Route::get('/testimonial/add', [\App\Http\Controllers\Admin\TestimonialController::class, 'add'])->name('testimonial.add');
Route::post('/testimonial/store', [\App\Http\Controllers\Admin\TestimonialController::class, 'store'])->name('testimonial.store');
Route::get('/testimonial/edit/{id}', [\App\Http\Controllers\Admin\TestimonialController::class, 'edit'])->name('testimonial.edit');
Route::post('/testimonial/update/{id}', [\App\Http\Controllers\Admin\TestimonialController::class, 'update'])->name('testimonial.update');
Route::get('/delete-testimonial/{id}', [\App\Http\Controllers\Admin\TestimonialController::class, 'delete'])->name('testimonial.delete');



// CMS Route
Route::get('/categories', [\App\Http\Controllers\Admin\CategoryController::class, 'index'])->name('category.index');
Route::post('/category/store', [\App\Http\Controllers\Admin\CategoryController::class, 'store'])->name('category.store');
Route::post('/category/update/{id}', [\App\Http\Controllers\Admin\CategoryController::class, 'update'])->name('category.update');
Route::get('/delete-category/{id}', [\App\Http\Controllers\Admin\CategoryController::class, 'delete'])->name('category.delete');


// Tag Route
Route::get('/tags', [\App\Http\Controllers\Admin\TagController::class, 'index'])->name('tag.index');
Route::post('/tag/store', [\App\Http\Controllers\Admin\TagController::class, 'store'])->name('tag.store');
Route::post('/tag/update/{id}', [\App\Http\Controllers\Admin\TagController::class, 'update'])->name('tag.update');
Route::get('/delete-tag/{id}', [\App\Http\Controllers\Admin\TagController::class, 'delete'])->name('tag.delete');





// Blog Route
Route::get('/blogs', [\App\Http\Controllers\Admin\BlogController::class, 'index'])->name('blog.index');
Route::get('/blog/add', [\App\Http\Controllers\Admin\BlogController::class, 'add'])->name('blog.add');
Route::post('/blog/store', [\App\Http\Controllers\Admin\BlogController::class, 'store'])->name('blog.store');
Route::get('/blog/edit/{id}', [\App\Http\Controllers\Admin\BlogController::class, 'edit'])->name('blog.edit');
Route::post('/blog/update/{id}', [\App\Http\Controllers\Admin\BlogController::class, 'update'])->name('blog.update');
Route::get('/delete-blog/{id}', [\App\Http\Controllers\Admin\BlogController::class, 'delete'])->name('blog.delete');




// Service Route
Route::get('/services', [\App\Http\Controllers\Admin\ServiceController::class, 'index'])->name('service.index');
Route::get('/service/add', [\App\Http\Controllers\Admin\ServiceController::class, 'add'])->name('service.add');
Route::post('/service/store', [\App\Http\Controllers\Admin\ServiceController::class, 'store'])->name('service.store');
Route::get('/service/edit/{id}', [\App\Http\Controllers\Admin\ServiceController::class, 'edit'])->name('service.edit');
Route::post('/service/update/{id}', [\App\Http\Controllers\Admin\ServiceController::class, 'update'])->name('service.update');
Route::get('/delete-service/{id}', [\App\Http\Controllers\Admin\ServiceController::class, 'delete'])->name('service.delete');


// Pricing Route
Route::get('/pricings', [\App\Http\Controllers\Admin\PricingController::class, 'index'])->name('pricing.index');
Route::get('/pricing/add', [\App\Http\Controllers\Admin\PricingController::class, 'add'])->name('pricing.add');
Route::post('/pricing/store', [\App\Http\Controllers\Admin\PricingController::class, 'store'])->name('pricing.store');
Route::get('/pricing/edit/{id}', [\App\Http\Controllers\Admin\PricingController::class, 'edit'])->name('pricing.edit');
Route::post('/pricing/update/{id}', [\App\Http\Controllers\Admin\PricingController::class, 'update'])->name('pricing.update');
Route::get('/delete-pricing/{id}', [\App\Http\Controllers\Admin\PricingController::class, 'delete'])->name('pricing.delete');


// Project Route
Route::get('/projects', [\App\Http\Controllers\Admin\ProjectController::class, 'index'])->name('project.index');
Route::get('/project/add', [\App\Http\Controllers\Admin\ProjectController::class, 'add'])->name('project.add');
Route::post('/project/store', [\App\Http\Controllers\Admin\ProjectController::class, 'store'])->name('project.store');
Route::get('/project/edit/{id}', [\App\Http\Controllers\Admin\ProjectController::class, 'edit'])->name('project.edit');
Route::post('/project/update/{id}', [\App\Http\Controllers\Admin\ProjectController::class, 'update'])->name('project.update');
Route::get('/delete-project/{id}', [\App\Http\Controllers\Admin\ProjectController::class, 'delete'])->name('project.delete');





    });

    Route::get('/logout', [\App\Http\Controllers\Admin\AdminLoginController::class, 'adminLogout'])->name('adminLogout');
});
